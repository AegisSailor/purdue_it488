<html>
 <head>
  <title>PHP Test</title>
 </head>
 <body>
    <?php
        // PHP Data Objects(PDO) Sample Code:
        try {
            $conn = new PDO("sqlsrv:server = tcp:pierson.database.windows.net,1433; Database = workOrderSystem", "rlpTest", "{your_password_here}");
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (PDOException $e) {
            print("Error connecting to SQL Server.");
            die(print_r($e));
        }
        
        // SQL Server Extension Sample Code:
        $connectionInfo = array("UID" => "rlpTest", "pwd" => "{1Purdue1}", "Database" => "workOrderSystem", "LoginTimeout" => 30, "Encrypt" => 1, "TrustServerCertificate" => 0);
        $serverName = "tcp:pierson.database.windows.net,1433";
        $conn = sqlsrv_connect($serverName, $connectionInfo);
    ?>
</body>
</html>



